import { useEffect, useRef } from 'react';
import {combineReducers, createStore} from 'redux';
import {incrementAction, decrementAction} from './store/actions/counter.action';
import { addTodoAction } from './store/actions/todos.action';
import counterReducer from './store/reducers/counter.reducer';
import todoscounterReducer from './store/reducers/todos.reducer';



const reducer = combineReducers({
  counter: counterReducer,
  todos: todoscounterReducer
});

const store = createStore(reducer);


function App() {
  const inputRef = useRef();

  useEffect(() => {
    store.subscribe(() => {
      
      const storeState = store.getState();
      console.log('state:', storeState);
    });
  }, [])
  

  function increment() {
    store.dispatch(incrementAction);
  }

  function decrement() {
    store.dispatch(decrementAction);
  }

  function addItem() {
    const todo = inputRef.current.value;
    const action = addTodoAction(todo);

    store.dispatch(action); 
  }

  return (
    <div>
      <button onClick={increment}>+1</button>
      <button onClick={decrement}>-1</button>
      <br/>
      <input ref={inputRef}/>
      <button onClick={addItem} >add</button>
    </div>
  );
}

export default App;
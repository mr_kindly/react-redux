export const incrementAction = { type: 'counter/incremented'}
export const decrementAction = { type: 'counter/decremented'}
export const addTodoActionType = 'todos/add';

export const addTodoAction = (value) => {
    return {type: addTodoActionType, payload: value}
};
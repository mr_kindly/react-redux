import {addTodoActionType} from './../actions/todos.action';

const initialState = {
    todos: []
  };
  
export default function todosReducer(state = initialState, action) {
    switch(action.type) {
      case addTodoActionType:
        return { todos: [...state.todos, action.payload]}
      default: 
        return state
    }
  }
import {incrementAction, decrementAction} from './../actions/counter.action'

const initialState = {
    value: 0
};

export default function counterReducer(state = initialState, action) {
    switch(action.type) {
      case incrementAction.type:
        const newState = { value: state.value + 1}
        return newState;
      case decrementAction.type:
        return { value: state.value - 1}
      
      default: 
        return state
    }
}